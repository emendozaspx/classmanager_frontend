const path = import('path');

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
    },
    devtool: "source-map",
    resolve: {
        extensions: [  ".js", ".json" ]
    },
    module: {
        rules: {

        }
    },
    devServer: {
        contentBase: __dirname
    },
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};
